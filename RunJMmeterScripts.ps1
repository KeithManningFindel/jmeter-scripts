# Note: 
# 1. This script is meant to be executed in the directory where JMeter.bat exists
# 2. The jmx files can be in another directory but they have to referenced properly as shown in the loop

###################
# Declare variables
###################

# Declare array with filenames of each JMeter test to run - JMeter file | short description | number of users
$testsArray = @( 	("LoginLogout", "Login and then logout", "10"),
					("AddToBasket", "Go to a product click to add to basket and then visit the basket", "18"), 
					("BasicSearch", "Perform 3 different searches", "10"), 
					("BasketPageLoadSpeedTest", "Go to 3 products click to add to basket go to basket page", "18"),
					("VisitHomepage", "Visit the homepage", "20"),
					("VisitProductDetailPage", "View product detail page for 25 products", "10"),
					("CategoriesSortByPrice", "Goto one category page and select option to sort by price (High/Low)", "20") )

# The directory where jmx files exist
$jmxFilesDirectory = "C:\Source\JMeter Scripts\"

# The command line to run JMeter and output results to jtl files and directories with web pages
$sampleCmdLine = ".\jmeter -n -t ""{jmx_files_directory}{jmx_test_file}.jmx"" -j results-{suffix_label}.jtl -l log-{suffix_label}.jtl" # -e -o dashboard-{suffix_label}"

# The array to hold the URLs to run the jmx scripts against, and a name to distinguish one from another
$urlsArray = @( ("uat.fecom-web.com", "uat"), 
                ("international-uat.fecom-web.com", "international") )


##########################################################				
# Delete old output folders and log files in JMeter folder
##########################################################

Remove-Item results-*.jtl -Force 
Remove-Item log-*.jtl -Force 
Remove-Item dashboard-* -Force -Recurse


#############################################################
# Run Jmeter for each URL and each JMeter test, generate logs
#############################################################

# Loop through all urls in the array
foreach ($url in $urlsArray)
{
	# Loop through input files in tests array
	foreach ($test in $testsArray)
	{
    	# read the contents of the next jmx file in the array
		$fullText = Get-Content ($jmxFilesDirectory+$test[0]+".jmx")
		#echo ($jmxFilesDirectory+$test[0]+".jmx") 

		# We need to replace and also read certain variables found in the JMeter test files like URLs, usernames, passwords, number of users, etc	
		
		# line 47 should contain the hostname url for all files so replace it
		$fullText[47] = "<stringProp name=""Argument.value"">"+$url[0]+"</stringProp>"	
		
		# If file is Login we need to update username and password
		if ($test[0] -eq "Login")
		{
			$fullText[53] = "<stringProp name=""Argument.value"">george.papoulakis@findel-education.co.uk</stringProp>"
			$fullText[58] = "<stringProp name=""Argument.value"">umbraco2017</stringProp>"
		}		
		
		# Save JMeter test file to a new output test file - overwrites old output test file
		Set-Content ($jmxFilesDirectory+$test[0]+"_changed.jmx") $fullText

		# Make copy of sample command line
		$tempCmdLine = $sampleCmdLine
		
		# Replace JMeter test filename and suffix labels in temp command line
		#$tempCmdLine = $tempCmdLine.replace("{test_filename}", $test[0]+"_changed")
		$tempCmdLine = $tempCmdLine.replace("{suffix_label}", $url[1]+"-"+$test[0])
		
		# Replace directory to read input JMeter test files
		$tempCmdLine = $tempCmdLine.replace("{jmx_files_directory}", $jmxFilesDirectory)
		
		# Replace input JMeter test file
		$tempCmdLine = $tempCmdLine.replace("{jmx_test_file}", $test[0])
		
		# Print the line to execute
		Write-Host $test[1] "on" $url[1]
		echo $tempCmdLine		
		echo "`n"    
		
		# Invoke the line
		iex $tempCmdLine
		
		# Delete temporary JMeter test file
		Remove-Item ($jmxFilesDirectory+$test[0]+"_changed.jmx") 
	}
}

###########################################				
# Generate array with results from the logs
###########################################

$resultsArray = [System.Collections.ArrayList]@()
#[void]$resultsArray.Add(@("Test steps", "Number of users", $urlsArray[0][1], $urlsArray[1][1]))

# Generate average values from the log files

# Loop through each test in tests array
foreach ($test in $testsArray)
{
	# Declare array to hold average values
	$resultsNumbers = [System.Collections.ArrayList]@()
	
	# Loop through all urls in the array
	foreach ($url in $urlsArray)
	{
		# Form the input log filename that resulted from current test
		$filename = "log-"+$url[1]+"-"+$test[0]+".jtl"
		
		# Read the log file
		$csvData = import-csv $filename
		
		# Gather average values from the log file
		$avgSum = 0; $lineCount = 0; $avg = 0
		foreach($line in $csvData)
		{
			$avgSum += $line.("Elapsed") 
			$lineCount++
		}
		$avgSum = $avgSum / 1000 # convert milliseconds to seconds
		$avg = [double]($avgSum / $lineCount) # get average number of seconds
		$avg = [math]::Round($avg, 2)
		
		# Store the average value
		[void]$resultsNumbers.Add($avg) # void stops from printing the array index		
	}
	
	[void]$resultsArray.Add(@($test[1], $test[2], $resultsNumbers[0], $resultsNumbers[1]))
}

# Print the results array 
foreach($row in $resultsArray)
{
	Write-Host $row[0] $row[1] $row[2] $row[3]
}

##########################
# Generate results in HTML
##########################

# Convert results to comma separated array 
$resultsTable =  $resultsArray  | % { $_ -join ','}

# Prepare a style for the HTML table
$Header = @"
<style>
TABLE {border-width: 1px; border-style: solid; border-color: #000; border-collapse: collapse;}
TH {border-width: 1px; padding: 3px; border-style: solid; border-color: #000; background-color: #000; color: #fff;}
TD {border-width: 1px; padding: 3px; border-style: solid; border-color: #000;}
</style>
"@

# Generate a filename for the results
$resultsFilename = "resultsTable_"+$(get-date -f yyyy-MM-dd_HH-mm-ss)+".html"

# Generate a results table in the form an HTML array with headers and style
$resultsTable | ConvertFrom-CSV -Header "Test steps", "Number of users", $urlsArray[0][1], $urlsArray[1][1] | ConvertTo-Html -Head $Header | Out-File -FilePath $resultsFilename




	

